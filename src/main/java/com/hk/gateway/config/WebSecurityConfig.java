package com.hk.gateway.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().ignoringAntMatchers("/eureka/**", "/hkSecurity/**", "/api/**", "/feed/**","/logistics/**","/crm/**");
		http.authorizeRequests().antMatchers("/health","/hkSecurity/**", "/api/**", "/feed/**","/logistics/**","/crm/**").
				permitAll().anyRequest().authenticated().and().formLogin().and().httpBasic();
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/api/**", "/feed/**","/logistics/**","/crm/**");
	}


}
