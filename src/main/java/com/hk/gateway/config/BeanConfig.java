package com.hk.gateway.config;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.hk.gateway.filter.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.commons.util.InetUtils;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
/*import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;*/

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.hk.gateway.response.SecurityResponseObj;
import com.hk.gateway.service.SecurityService;
import com.netflix.appinfo.AmazonInfo;
import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;

@Configuration
public class BeanConfig {

	@Autowired
	SecurityService securityService;

	@Bean
	public ServletRegistrationBean hystrixStreamServlet() {
		return new ServletRegistrationBean(new HystrixMetricsStreamServlet(), "/hystrix.stream");
	}

	@Bean
	@Profile("prod")
	public EurekaInstanceConfigBean eurekaInstanceConfig(InetUtils inetUtils) {
		EurekaInstanceConfigBean b = new EurekaInstanceConfigBean(inetUtils);
		AmazonInfo info = AmazonInfo.Builder.newBuilder().autoBuild("eureka");
		b.setDataCenterInfo(info);
		return b;
	}

	/*@Bean
	@Profile("prod")
	public UserDetailsService userDetailsService() {
		UserDetails user = User.withDefaultPasswordEncoder().username("user").password("user").roles("USER").build();
		return new InMemoryUserDetailsManager(user);
	}*/

	/*@Bean
	public LoadingCache<Object, SecurityResponseObj> cacheLoader() {
		LoadingCache<Object, SecurityResponseObj> caches = Caffeine.newBuilder().refreshAfterWrite(10, TimeUnit.SECONDS)
				.build(key -> securityService.getAppConfig());
		return caches;
	}*/

	/*
	 * //For future use
	 * 
	 * @Bean public RoutingFilter addRoutingFilter() { return new RoutingFilter(); }
	 */

	@Bean
	public HKSecurityFilter addHKSecurityFilter() {
		return new HKSecurityFilter();
	}

	@Bean
	public CrmHKSecurityFilter addCrmHKSecurityFilter() {
		return new CrmHKSecurityFilter();
	}

	@Bean
	public SecureAuthFilter addSecureAuthFilter() {
		return new SecureAuthFilter();
	}

	@Bean
	public HkHttpRequestFilter addHkHttpRequestFilter() {
		return new HkHttpRequestFilter();
	}

	@Bean
	public RestAuthenticationFilter addRestAuthenticationFilter() {
		return new RestAuthenticationFilter();
	}
	
	@Bean
	public ApiValidatedFilter apiValidatedFilter() {
		return new ApiValidatedFilter();
	}
}
