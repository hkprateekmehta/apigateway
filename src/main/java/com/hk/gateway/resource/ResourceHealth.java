package com.hk.gateway.resource;

import com.hk.gateway.service.SecurityBypassedService;
import com.hk.gateway.service.SecurityService;
import com.hk.gateway.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;

/**
 * Created by dhruv on 9/5/18.
 */
@RestController
public class ResourceHealth {

    public static final long ADMIN_USER_ID = 1L;
    private static volatile boolean isStartup=true;

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private SecurityBypassedService securityBypassedService;

    @GET
    @RequestMapping(value = "/health")
    public String secureGetRecentOrdersForUserInStore() {
        boolean doesUserExists = userService.doesUserExistById(ADMIN_USER_ID);
        return String.valueOf(doesUserExists);
    }

    @GET
    @RequestMapping(value = "/refreshSecurityBypassed")
    public String refreshSecurityBypassed() {
        securityBypassedService.refreshAllActiveSet();
        return "Security Bypassed Cache Reloaded";
    }

    @GET
    @RequestMapping(value = "/refreshHKSecurity")
    public String refreshHKSecurity() {
        securityService.reloadSecurityCacheOnCluster();
        securityService.reloadSecurityCRMCacheOnCluster();
        return "Security Cache Reloaded";
    }

}
