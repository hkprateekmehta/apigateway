package com.hk.gateway.pojo;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_application")
public class UserApplication {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", unique = true, nullable = false)
  private Long id;

  @Column(name = "user_id")
  private Long userId;

  @Column(name = "user_vsn_id")
  private String userVersionId;

  @Column(name = "gcm_id")
  private String gcmId;

  @Column(name = "email")
  private String email;

  @Column(name = "device_id")
  private String deviceId;

  @Column(name = "device_name")
  private String deviceName;

  @Column(name = "device_brand")
  private String deviceBrand;

  @Column(name = "google_adv_id")
  private String googleAdvId;

  @Column(name = "first_used_network")
  private String firstUsedNetwork;

  @Column(name = "last_used_network")
  private String lastUsedNetwork;

  @Column(name = "android_vsn")
  private String androidVersion;

  @Column(name = "last_activity_date")
  private Date lastActivityDate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "gcm_create_date", nullable = false, length = 19)
  private Date createDate = new Date();

  @Column(name = "platform_id")
  private Long platformId;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "update_date", updatable = true)
  private Date updateDate;

  @Column(name = "last_ping_status")
  private Boolean lastPingStatus;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "last_fail_ping_date")
  private Date lastFailPingDate;

  @Column(name = "store_id")
  private Long storeId;

  @Column(name="new_last_ping_status")
  private Integer newLastPingStatus;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserVersionId() {
    return userVersionId;
  }

  public void setUserVersionId(String userVersionId) {
    this.userVersionId = userVersionId;
  }

  public String getGcmId() {
    return gcmId;
  }

  public void setGcmId(String gcmId) {
    this.gcmId = gcmId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public String getDeviceName() {
    return deviceName;
  }

  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }

  public String getDeviceBrand() {
    return deviceBrand;
  }

  public void setDeviceBrand(String deviceBrand) {
    this.deviceBrand = deviceBrand;
  }

  public String getGoogleAdvId() {
    return googleAdvId;
  }

  public void setGoogleAdvId(String googleAdvId) {
    this.googleAdvId = googleAdvId;
  }

  public String getFirstUsedNetwork() {
    return firstUsedNetwork;
  }

  public void setFirstUsedNetwork(String firstUsedNetwork) {
    this.firstUsedNetwork = firstUsedNetwork;
  }

  public String getLastUsedNetwork() {
    return lastUsedNetwork;
  }

  public void setLastUsedNetwork(String lastUsedNetwork) {
    this.lastUsedNetwork = lastUsedNetwork;
  }

  public String getAndroidVersion() {
    return androidVersion;
  }

  public void setAndroidVersion(String androidVersion) {
    this.androidVersion = androidVersion;
  }

  public Date getLastActivityDate() {
    return lastActivityDate;
  }

  public void setLastActivityDate(Date lastActivityDate) {
    this.lastActivityDate = lastActivityDate;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Long getPlatformId() {
    return platformId;
  }

  public void setPlatformId(Long platformId) {
    this.platformId = platformId;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public Boolean getLastPingStatus() {
    return lastPingStatus;
  }

  public void setLastPingStatus(Boolean lastPingStatus) {
    this.lastPingStatus = lastPingStatus;
  }

  public Date getLastFailPingDate() {
    return lastFailPingDate;
  }

  public void setLastFailPingDate(Date lastFailPingDate) {
    this.lastFailPingDate = lastFailPingDate;
  }

  public Long getStoreId() {
    return storeId;
  }

  public void setStoreId(Long storeId) {
    this.storeId = storeId;
  }

  public Integer getNewLastPingStatus() {
    return newLastPingStatus;
  }

  public void setNewLastPingStatus(Integer newLastPingStatus) {
    this.newLastPingStatus = newLastPingStatus;
  }
}
