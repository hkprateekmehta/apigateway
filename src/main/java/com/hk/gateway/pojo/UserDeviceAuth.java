package com.hk.gateway.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "user_device_auth")
public class UserDeviceAuth implements Serializable {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "user_id", unique = true, nullable = false)
  private Long userId;

  @Column(name = "auth_token", unique = true, nullable = false)
  private String authToken;

  @Column(name = "store_id")
  private Long storeId;

  @Column(name = "platform")
  private Long platform;

  @Column(name = "device_id")
  private String deviceId;


  @Column(name = "last_login_dt")
  private Date lastLoginDate;

  @Column(name = "logged_in")
  private Boolean loggedIn;

  @Column(name = "expiry_dt", nullable = false)
  private Date expiryDate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_dt", nullable = false, length = 19)
  private Date createDate = new Date();


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getAuthToken() {
    return authToken;
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }

  public Long getPlatform() {
    return platform;
  }

  public void setPlatform(Long platform) {
    this.platform = platform;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public Date getLastLoginDate() {
    return lastLoginDate;
  }

  public void setLastLoginDate(Date lastLoginDate) {
    this.lastLoginDate = lastLoginDate;
  }

  public Date getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Long getStoreId() {
    return storeId;
  }

  public void setStoreId(Long storeId) {
    this.storeId = storeId;
  }

  public Boolean getLoggedIn() {
    return loggedIn;
  }

  public void setLoggedIn(Boolean loggedIn) {
    this.loggedIn = loggedIn;
  }
}
