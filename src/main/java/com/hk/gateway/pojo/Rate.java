package com.hk.gateway.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "rate")
public class Rate implements Serializable {

  @Id
  @Column(name = "rate_key", nullable = false)
  private String rateKey;

  @Column(name = "remaining")
  private Long remaining;

  @Column(name = "reset")
  private Long reset;

  public String getRateKey() {
    return rateKey;
  }

  public void setRateKey(String rateKey) {
    this.rateKey = rateKey;
  }

  public Long getRemaining() {
    return remaining;
  }

  public void setRemaining(Long remaining) {
    this.remaining = remaining;
  }


  public Long getReset() {
    return reset;
  }

  public void setReset(Long reset) {
    this.reset = reset;
  }

}