package com.hk.gateway.constant;

public class GatewayApplicationConstants {

    public static final String API_REQUEST_URI = "/api/";
    public static final String LOGISTICS_REQUEST_URI = "/logistics/";
    public static final String CRM_REQUEST_URI = "/crm/";
}
