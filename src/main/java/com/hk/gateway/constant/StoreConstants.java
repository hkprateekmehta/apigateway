package com.hk.gateway.constant;


public class StoreConstants {

  public final static String USER_DEVICE_AUTH="USER_DEVICE_AUTH";
  public final static Long DEFAULT_PLATFORM_ID=3L;
  
  public final static Long DEFAULT_STORE_ID=1L;
  public final static String HAPTIK_CLIENT_ID="HAPTIK_CLIENT_ID";

  public final static String USER_IDS_FOR_APP_DEBUG="USER_IDS_FOR_APP_DEBUG";

}
