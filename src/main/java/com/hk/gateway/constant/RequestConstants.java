package com.hk.gateway.constant;

public class RequestConstants {
	public static final String USER_ID = "userId";
	public static final String PLATFORM = "plt";
	public static final String HEADER_APP_VERSION_ID = "version";
	public static final String CLIENT_ID = "clientId";
	public static final String X_FORWARDED_FOR = "X-FORWARDED-FOR";
	public static final String AUTHENTICATION_HEADER = "Authorization";
	public static final String HEADER_DEVICE_ID = "deviceId";
	public static final String STORE_ID = "st";
	public static final String USER_AGENT = "user-agent";

}
