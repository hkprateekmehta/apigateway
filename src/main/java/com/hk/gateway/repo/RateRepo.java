package com.hk.gateway.repo;

import com.hk.gateway.pojo.Rate;
import com.hk.gateway.pojo.UserDeviceAuth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateRepo extends JpaRepository<Rate, Long> {
	
}
