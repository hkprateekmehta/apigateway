package com.hk.gateway.repo;

import com.hk.gateway.pojo.UserDeviceAuth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDeviceAuthRepo extends JpaRepository<UserDeviceAuth, Long> {
	public UserDeviceAuth findByAuthTokenAndDeviceIdAndUserId(String authToken,String deviceId,Long userId);
}
