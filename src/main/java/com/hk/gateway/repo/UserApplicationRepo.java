package com.hk.gateway.repo;

import com.hk.gateway.pojo.UserApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserApplicationRepo extends JpaRepository<UserApplication, Long> {

	@Query(nativeQuery = true, value ="SELECT * FROM user_application WHERE user_id= :userId and user_vsn_id= :appVersion and (last_ping_status=1 OR last_ping_status IS NULL) ORDER BY update_date DESC LIMIT 1")
	public UserApplication findByUserIdAnduserVersionIdAndlastPingStatusOrderByUpdateDateDesc(Long userId, String appVersion);
}
