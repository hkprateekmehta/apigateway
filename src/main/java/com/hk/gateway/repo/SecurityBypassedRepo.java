package com.hk.gateway.repo;

import com.hk.gateway.pojo.SecurityBypassed;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SecurityBypassedRepo extends JpaRepository<SecurityBypassed, Long> {

}

