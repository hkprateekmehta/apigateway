package com.hk.gateway.repo;

import com.hk.gateway.pojo.UserAuth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAuthRepo extends JpaRepository<UserAuth, Long> {
	public UserAuth findByAuthToken(String authToken);
	public UserAuth findByAuthTokenAndUserId(String authToken,Long userId);
}
