package com.hk.gateway.service;

import com.hk.gateway.dto.UserAuthDTO;
import com.hk.gateway.pojo.UserAuth;
import com.hk.gateway.pojo.UserDeviceAuth;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vidur on 11/18/2015.
 */
public interface SSOService {

	/*
	 * public SSOUserResponse getSSOUserResponse(SSOUserRequest ssoUserRequest);
	 * 
	 *//**
		 * @param user
		 * @param authExpirationDate
		 * @param updateToken
		 *            - to regenerate token --> used in case of password change
		 *            so that token is updated according to new password
		 * @return
		 */
	/*
	 * public UserAuth createUpdateAuthToken(User user, Date authExpirationDate,
	 * boolean updateToken);
	 * 
	 *//**
		 * this api will return user basic info based on user auth token(used in
		 * SSO)
		 *
		 * @param userInfoRequest
		 * @return
		 *//*
		 * public UserInfoResponse getUserResponseByAuthToken(UserInfoRequest
		 * userInfoRequest);
		 */

	public UserAuthDTO getUserAuthByToken(String authToken);

    public UserAuthDTO getAuthToken(String authToken, String deviceId, HttpServletRequest httpServletRequest, Long userId,Long storeId, Long platform,String version,String userAgent);

	public UserDeviceAuth getUserDeviceAuthByTokenAndDevice(String authToken, String deviceId, Long userId);

	public Long getRequestStore(HttpServletRequest request);

	public Long getRequestPlatform(HttpServletRequest request);

	public Boolean getUserDeviceAuthStore(Long storeIdL);

	public String getUserIdsToPrintLogs(Long storeIdL);

	/*
	 * public UserAuthTokenResponse getAuthTokenResponseForUser(Long userId,
	 * Long storeId);
	 * 
	 * public UserAuth getAuthTokenForUser(Long userId);
	 */
}
