
package com.hk.gateway.service;

import com.hk.gateway.client.SecurityFeignClient;
import com.hk.gateway.response.SecurityResponseObj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;

@Service
@EnableScheduling
public class SecurityService {

	private static Logger logger = LoggerFactory.getLogger(SecurityService.class);
	private SecurityResponseObj securityResponseObj;
	private SecurityResponseObj logisticsSecurityResponseObj;
    private SecurityResponseObj crmSecurityResponseObj;

	@Value("${apiApplicationId}")
	private String apiApplicationId;

	@Value("${logisticsApplicationId}")
	private String logisticsApplicationId;

    @Value("${crmApplicationId}")
    private String crmApplicationId;

	@Value("${useHkSecurity}")
	private String useHkSecurity;

    @Autowired
    private SecurityFeignClient client;

	@Scheduled(fixedDelay = 300000, initialDelay = 3000)
	public void reloadSecurityCacheOnCluster() {
		logger.info("Inside reloadSecurityCacheOnCluster");

		try {
			if (!checkUseHkSecurity()) {
				logger.info("HKSecurity disabled useHkSecurity : " + useHkSecurity);
				return;
			}
			//logger.info("Going to fetch Security data");
			SecurityResponseObj response = getAppConfig(apiApplicationId);

			if (response != null) {
				securityResponseObj = response;
				logger.info("Security data received successfully");

			} else {
				logger.info("No Security data returned!");
			}
		} catch (Exception e) {
			logger.error("Exception while fetching Security data", e);
		}
	}

    @Scheduled(fixedDelay = 300000, initialDelay = 3000)
    public void reloadSecurityCRMCacheOnCluster() {

        try {
            if (!checkUseHkSecurity()) {
                logger.info("reloadSecurityCRMCacheOnCluster HKSecurity disabled useHkSecurity : " + useHkSecurity);
                return;
            }
            //logger.info("Going to fetch Security data");
            SecurityResponseObj response = getAppConfig(crmApplicationId);

            if (response != null) {
                crmSecurityResponseObj = response;
                logger.info("reloadSecurityCRMCacheOnCluster Security data received successfully");

            } else {
                logger.info("reloadSecurityCRMCacheOnCluster No Security data returned!");
            }
        } catch (Exception e) {
            logger.error("reloadSecurityCRMCacheOnCluster, Exception while fetching Security data", e);
        }
    }

	@Scheduled(fixedDelay = 300000, initialDelay = 6000)
	public void reloadSecurityLogisticsCacheOnCluster() {
		logger.info("Inside Logistics reloadSecurityCacheOnCluster");

		try {
			if (!checkUseHkSecurity()) {
				logger.info("HKSecurity disabled useHkSecurity : " + useHkSecurity);
				return;
			}
			//logger.info("Going to fetch Security data");
			SecurityResponseObj response = getAppConfig(logisticsApplicationId);

			if (response != null) {
				logisticsSecurityResponseObj = response;
				logger.info("Logistics Security data received successfully");

			} else {
				logger.info("No Logistics Security data returned!");
			}
		} catch (Exception e) {
			logger.error("Exception while fetching Logistics Security data", e);
		}
	}

	public boolean checkClientIP(String clientId, String clientIP, Map<String, Set<String>> clientIdAndClientIPsMapping) {
		Set<String> clientIps = clientIdAndClientIPsMapping.get(clientId);
		if (clientIps == null || clientIps.isEmpty()) {
			return true;
		}
		String[] clientIPArray = clientIP.split(",");
		for (String clientIP3 : clientIPArray) {
			int endIndex = clientIP3.lastIndexOf(".");
			clientIP3 = clientIP3.substring(0, endIndex);
			if (clientIps.contains(clientIP3)) {
				return true;
			}
		}
		return false;

	}

	public boolean checkStarApi(Set<String> starOpenApiSet, int clientAPIArrayLength, String[] clientAPIArray) {
		if (starOpenApiSet != null && starOpenApiSet.size() > 0) {

			for (String starOpenApi : starOpenApiSet) {
				String[] starOpenApiArray = starOpenApi.split("/");
				int count = 0;
				for (int i = 0; i < clientAPIArrayLength; i++) {
					if (clientAPIArray[i].equalsIgnoreCase(starOpenApiArray[i])
							|| starOpenApiArray[i].equals("*")) {
						count++;
					} else {
						break;
					}
				}
				if (count == clientAPIArrayLength) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkStarInternalApi(String clientId, String[] clientAPIArray,
										 int clientAPIArrayLength, Map<String, Map<Integer, Set<String>>> starClientIdAndClientAPIsMapping) {
		Map<Integer, Set<String>> starClientAPIMap = starClientIdAndClientAPIsMapping.get(clientId);
		if (starClientAPIMap != null && starClientAPIMap.size() > 0) {
			Set<String> starClientAPISet = starClientAPIMap.get(clientAPIArrayLength);
			return checkStarApi(starClientAPISet, clientAPIArrayLength, clientAPIArray);
		}
		return false;
	}


	public boolean checkNonStarInternalAPI(String clientId, String clientAPITrimmed,
											int clientAPIArrayLength, Map<String, Map<Integer, Set<String>>> nonStarClientIdAndClientAPIsMapping) {
		Map<Integer, Set<String>> nonStarClientAPIMap = nonStarClientIdAndClientAPIsMapping.get(clientId);
		if (nonStarClientAPIMap != null && nonStarClientAPIMap.size() > 0) {
			Set<String> nonStarClientAPISet = nonStarClientAPIMap.get(clientAPIArrayLength);
			return (nonStarClientAPISet != null && nonStarClientAPISet.size() > 0
					&& nonStarClientAPISet.contains(clientAPITrimmed));
		}
		return false;
	}


	public boolean checkClientApiInNonStarOpenApi(String clientAPITrimmed, int clientAPIArrayLength, Map<Integer, Set<String>> nonStarOpenApiMap) {
		if (nonStarOpenApiMap != null && nonStarOpenApiMap.size() > 0) {
			Set<String> nonStarOpenApiSet = nonStarOpenApiMap.get(clientAPIArrayLength);
			return nonStarOpenApiSet != null && nonStarOpenApiSet.size() > 0
					&& nonStarOpenApiSet.contains(clientAPITrimmed);
		}
		return false;
	}

	public boolean checkWithStarOpenApi(String[] clientAPIArray, int clientAPIArrayLength, Map<Integer, Set<String>> starOpenApiMap) {
		if (starOpenApiMap != null && starOpenApiMap.size() > 0) {
			Set<String> starOpenApiSet = starOpenApiMap.get(clientAPIArrayLength);
			return checkStarApi(starOpenApiSet, clientAPIArrayLength, clientAPIArray);
		}
		return false;
	}

	//@Cacheable("appConfig")
	private SecurityResponseObj getAppConfig(String applicationId) {
		return client.getApplicationConfiguration(applicationId);
	}

	public SecurityResponseObj getSecurityResponseObj() {
		if (securityResponseObj == null) {
			reloadSecurityCacheOnCluster();
		}
		return securityResponseObj;
	}

	public SecurityResponseObj getLogisticsResponseObj() {
		if (logisticsSecurityResponseObj == null) {
			reloadSecurityLogisticsCacheOnCluster();
		}
		return logisticsSecurityResponseObj;
	}

    public SecurityResponseObj getCrmSecurityResponseObj() {
        if (crmSecurityResponseObj == null) {
            reloadSecurityCRMCacheOnCluster();
        }
        return crmSecurityResponseObj;
    }

    public boolean checkUseHkSecurity() {
	    return ("true").equals(useHkSecurity);
	}

}
