package com.hk.gateway.service.impl;

import com.hk.gateway.pojo.SecurityBypassed;
import com.hk.gateway.repo.SecurityBypassedRepo;
import com.hk.gateway.service.SecurityBypassedService;
import com.hk.gateway.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@EnableScheduling
public class SecurityBypassedServiceImpl implements SecurityBypassedService {

    @Autowired
    private SecurityBypassedRepo securityBypassedRepo;

    @Autowired
    private SecurityService securityService;

    private Set<String> securityBypassedNonStarSet = new HashSet<>();
    private Set<String> securityBypassedStarSet = new HashSet<>();

    public List<SecurityBypassed> getAll(){
        return securityBypassedRepo.findAll();
    }

    public boolean checkForSecurityBypass(String requestURI, int clientAPIArrayLength, String[] clientAPIArray){
        if(securityBypassedNonStarSet.contains(requestURI)){
            return true;
        }
        return securityService.checkStarApi(securityBypassedStarSet,clientAPIArrayLength,clientAPIArray);
    }

    @Scheduled(fixedDelay = 300000, initialDelay = 3000)
    public void refreshAllActiveSet(){
        List<SecurityBypassed> securityBypassedList =  getAll();
        Set<String> refreshedNonStarSet = new HashSet<>();
        Set<String> refreshedStarSet = new HashSet<>();
        if(securityBypassedList!=null && !securityBypassedList.isEmpty()) {
            for (SecurityBypassed securityBypassed : securityBypassedList) {
                if (securityBypassed.isActive()) {
                    if(securityBypassed.getApiURI().contains("*")){
                        refreshedStarSet.add(securityBypassed.getApiURI());
                    }else{
                        refreshedNonStarSet.add(securityBypassed.getApiURI());
                    }

                }
            }
        }
        securityBypassedNonStarSet = refreshedNonStarSet;
        securityBypassedStarSet = refreshedStarSet;
    }

}
