package com.hk.gateway.service.impl;

import com.hk.gateway.constant.RequestConstants;
import com.hk.gateway.constant.StoreConstants;
import com.hk.gateway.dto.UserAuthDTO;
import com.hk.gateway.filter.SecureAuthFilter;
import com.hk.gateway.pojo.*;
import com.hk.gateway.repo.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hk.gateway.exception.InvalidParameterException;
import com.hk.gateway.service.SSOService;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Date;

@Service
public class SSOServiceImpl implements SSOService {

	@Autowired
	private UserAuthRepo repo;
	@Autowired
	private UserDeviceAuthRepo deviceRepo;
	@Autowired
	private UserApplicationRepo applicationRepo;
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private StoreConfigValuesRepo storeConfigValuesRepo;

	private static Logger logger = LoggerFactory.getLogger(SSOServiceImpl.class);

	@Override
	public UserAuthDTO getUserAuthByToken(String authToken) {
		UserAuthDTO userAuthDto = null;
		if (StringUtils.isBlank(authToken)) {
			throw new InvalidParameterException("ACCESS_TOKEN_CANNOT_BE_BLANK");
		}

		 UserAuth userAuth=repo.findByAuthToken(authToken);
		if(userAuth!=null){
			userAuthDto = new UserAuthDTO();
			userAuthDto.setAuthToken(userAuth.getAuthToken());
			userAuthDto.setExpiryDate(userAuth.getExpiryDate());
			userAuthDto.setUserId(userAuth.getUserId());
		}
		return userAuthDto;

	}

	@Override
	public UserAuthDTO getAuthToken(String authToken, String deviceId, HttpServletRequest httpServletRequest,
																	Long userId,Long storeId, Long platform,String appVersion, String userAgent) {

		UserAuthDTO userAuthDto = null;

		if(deviceId == null){

			logger.info("In getAuthToken, Device Id is NULL for userId "+userId+" for store "
					+storeId+" platform "+platform+" and appVersion "+appVersion+" URL "+httpServletRequest.getRequestURL()
					+" and Method "+httpServletRequest.getMethod() +"and user-agent:"+userAgent+"  and Content type "
					+httpServletRequest.getContentType());
			/*if(platform !=null && platform.equals(StoreConstants.DEFAULT_PLATFORM_ID)){
				UserApplication ua=applicationRepo.findByUserIdAnduserVersionIdAndlastPingStatusOrderByUpdateDateDesc(userId, appVersion);
				if(ua!=null){
					deviceId=ua.getDeviceId();
				}
			}*/
		}

		UserDeviceAuth userDeviceAuth = getUserDeviceAuthByTokenAndDevice(authToken,deviceId,userId);
		if(userDeviceAuth == null){
			userAuthDto = getUserAuthByTokenAndUserId(authToken,userId);
			if (userAuthDto != null){
				userAuthDto.setStore(storeId);
				saveUserAuthToUserDeviceAuth(authToken,deviceId,httpServletRequest,userId,userAuthDto.getExpiryDate()
						,storeId,platform);
			}

		}else{
			if(userDeviceAuth.getLoggedIn()) {
				userAuthDto = new UserAuthDTO();
				userAuthDto.setAuthToken(userDeviceAuth.getAuthToken());
				userAuthDto.setExpiryDate(userDeviceAuth.getExpiryDate());
				userAuthDto.setUserId(userDeviceAuth.getUserId());
				userAuthDto.setStore(userDeviceAuth.getStoreId());
			}
		}
		return userAuthDto;
	}

	private UserAuthDTO getUserAuthByTokenAndUserId(String authToken, Long userId) {
		UserAuthDTO userAuthDto = null;
		if (StringUtils.isBlank(authToken)) {
			throw new InvalidParameterException("ACCESS_TOKEN_CANNOT_BE_BLANK");
		}

		UserAuth userAuth=repo.findByAuthTokenAndUserId(authToken,userId);
		if(userAuth!=null){
			userAuthDto = new UserAuthDTO();
			userAuthDto.setAuthToken(userAuth.getAuthToken());
			userAuthDto.setExpiryDate(userAuth.getExpiryDate());
			userAuthDto.setUserId(userAuth.getUserId());
		}
		return userAuthDto;

	}

	private void saveUserAuthToUserDeviceAuth(String authToken, String deviceId, HttpServletRequest request, Long userId, Date expiryDate,Long storeId, Long platformId) {
		if(platformId == null){
			platformId=getRequestPlatform(request);
		}
		if(storeId == null) {
			storeId=getRequestStore(request);
		}

		UserDeviceAuth userDeviceAuth = new UserDeviceAuth();
		userDeviceAuth.setUserId(userId);
		userDeviceAuth.setAuthToken(authToken);
		userDeviceAuth.setPlatform(platformId);
		userDeviceAuth.setStoreId(storeId);
		userDeviceAuth.setDeviceId(deviceId);
		userDeviceAuth.setLastLoginDate(new Date());
		userDeviceAuth.setLoggedIn(true);
		userDeviceAuth.setExpiryDate(expiryDate);

		deviceRepo.save(userDeviceAuth);
	}

	@Override
	@Transactional
	public UserDeviceAuth getUserDeviceAuthByTokenAndDevice(String authToken, String deviceId, Long userId) {
		if (StringUtils.isBlank(authToken)) {
			throw new InvalidParameterException("ACCESS_TOKEN_CANNOT_BE_BLANK");
		}

		return deviceRepo.findByAuthTokenAndDeviceIdAndUserId(authToken,deviceId,userId);

	}

	@Override
	public Long getRequestStore(HttpServletRequest request) {
		Long storeId = null;
		try {
			if (request.getMethod().equalsIgnoreCase("POST")) {
				if (request.getContentType().equalsIgnoreCase("application/json")) {

					String httpRequestBody = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
					JSONObject jsonObject = new JSONObject(httpRequestBody);
					if (jsonObject.has("storeId")) {
						//getting storeId from POST Request
						storeId = Long.parseLong(jsonObject.get("storeId").toString());
					}
				}

			} else if (request.getMethod().equalsIgnoreCase("GET") && request.getParameter("st")!=null) {
				//getting storeId  from GET Request
				storeId = Long.parseLong(request.getParameter("st"));

			}
			if(storeId == null){
				storeId = getRequestStoreFromUser(request);
			}
			if(storeId == null){
				logger.info("In getAuthToken, Store Id is NULL for URL"+request.getRequestURL()
						+" and Method "+request.getMethod() +" and Content type "+request.getContentType());
			}
		}catch(Exception e){
			logger.error("In getRequestStore",e);
		}
		return storeId;
	}

	private Long getRequestStoreFromUser(HttpServletRequest request) {
		Long storeId = null;
		try {
			String headerUserId = request.getHeader(RequestConstants.USER_ID);
			if(headerUserId != null) {
				User user = userRepo.findById(Long.parseLong(headerUserId)).get();
				storeId = user.getStoreId();
			}
		}catch(Exception e){
			logger.error("In getRequestStoreFromUser",e);
		}
		return storeId;
	}

	@Override
	public Long getRequestPlatform(HttpServletRequest request) {
		Long platform=null;
		try {
			if (request.getMethod().equalsIgnoreCase("POST")) {
				if (request.getContentType().equalsIgnoreCase("application/json")) {

					String httpRequestBody = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
					JSONObject jsonObject = new JSONObject(httpRequestBody);
					if (jsonObject.has("platformId")) {
						//getting platformId from POST Request
						platform = Long.parseLong(jsonObject.get("platformId").toString());
					}
				}

			} else if (request.getMethod().equalsIgnoreCase("GET") && request.getParameter("plt")!=null) {
				//getting  PlatformId from GET Request
				platform = Long.parseLong(request.getParameter("plt"));

			}
			if(platform==null){
				logger.info("In getAuthToken, platform Id is NULL for URL"+request.getRequestURL()+" and Method "+request.getMethod() +" and Content type "+request.getContentType());
				platform=StoreConstants.DEFAULT_PLATFORM_ID;
			}
		}catch(Exception e){
			logger.error("In getRequestPlatform",e);
		}
		return platform;
	}

	@Override
	public Boolean getUserDeviceAuthStore(Long storeIdL) {
		Boolean isDeviceAuthStore = false;
		try {
			StoreConfigValues storeConfigValues = storeConfigValuesRepo.findByConfigKeyAndStoreId(StoreConstants.USER_DEVICE_AUTH,storeIdL);
			if (storeConfigValues != null && "true".equalsIgnoreCase(storeConfigValues.getConfigValue())) {
				isDeviceAuthStore=true;
			}
		} catch (Exception e) {
			logger.error("Critical Exception in getUserDeviceAuthStore.", e);
			return false;
		}
		return isDeviceAuthStore;
	}

	@Override
	public String getUserIdsToPrintLogs(Long storeIdL) {
		StoreConfigValues storeConfigValues = storeConfigValuesRepo.findByConfigKeyAndStoreId(StoreConstants.USER_IDS_FOR_APP_DEBUG,storeIdL);
		if(storeConfigValues != null){
			return storeConfigValues.getConfigValue();
		}
		return null;
	}

}
