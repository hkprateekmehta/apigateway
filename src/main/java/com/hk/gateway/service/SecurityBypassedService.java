package com.hk.gateway.service;

import com.hk.gateway.pojo.SecurityBypassed;

import java.util.List;
import java.util.Set;

public interface SecurityBypassedService {

	List<SecurityBypassed> getAll();

	boolean checkForSecurityBypass(String requestURI, int clientAPIArrayLength, String[] clientAPIArray);

	void refreshAllActiveSet();

}
