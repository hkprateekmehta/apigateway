package com.hk.gateway.filter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hk.gateway.constant.*;
import com.hk.gateway.dto.UserAuthDTO;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.hk.gateway.common.MultiReadHttpServletRequest;
import com.hk.gateway.pojo.User;
import com.hk.gateway.pojo.UserAuth;
import com.hk.gateway.security.Crypt;
import com.hk.gateway.service.AgentHasUserService;
import com.hk.gateway.service.SSOService;
import com.hk.gateway.service.UserService;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

/**
 * this filer checks if the request is authenticated or not there are 2 headers
 * - authorization and user id authorization is a combination of access token
 * and user id (encrypted) user id is the id of the user for which the data is
 * being requested/updated there are 3 checks in this filter - matching
 * authorization header ..i.e., matching access token with user 2nd check - user
 * id in authorization header matches with the user id in user id header - this
 * user id will be used to fetch/update all data related to user 3rd check - if
 * request method is POST, compare body-userId and authorization-userId
 */
public class SecureAuthFilter extends ZuulFilter {
	public static final String AUTHENTICATION_HEADER = "Authorization";

	public static final String UNAUTH_MSG_TEMPLETE = "REQUEST UNAUTHORIZED FOR USER ";
	public static final String USER_ID_DOESNT_EXISTS = "USER DOESN'T EXIST";

	private static Logger logger = LoggerFactory.getLogger(SecureAuthFilter.class);

	@Autowired
	private SSOService ssoService;
	@Autowired
	private UserService userService;
	@Autowired
	private AgentHasUserService agentHasUserService;

	public boolean shouldFilter() {
		String requestURI = RequestContext.getCurrentContext().getRequest().getRequestURI();
		HttpServletRequest httpServletRequest = RequestContext.getCurrentContext().getRequest();
		String authHeader = httpServletRequest.getHeader(AUTHENTICATION_HEADER);
		return (requestURI.contains("/v3/") && requestURI.startsWith("/api/") )
				|| ( requestURI.startsWith("/crm/")
				&& (requestURI.startsWith("/crm/") && StringUtils.isNotBlank(authHeader)));
	}

	public Object run() throws ZuulException {

		RequestContext context = RequestContext.getCurrentContext();
		ServletRequest request = context.getRequest();
		if (request  != null) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			String authCredentials = httpServletRequest.getHeader(AUTHENTICATION_HEADER);
			String headerUserId = httpServletRequest.getHeader(RequestConstants.USER_ID);
			String platformId = httpServletRequest.getHeader(RequestConstants.PLATFORM);
			String version = httpServletRequest.getHeader(RequestConstants.HEADER_APP_VERSION_ID);
			String deviceId = httpServletRequest.getHeader(RequestConstants.HEADER_DEVICE_ID);
			String storeId =httpServletRequest.getHeader(RequestConstants.STORE_ID);
			String userAgent =httpServletRequest.getHeader(RequestConstants.USER_AGENT);

			Long storeIdL = null, platformIdL = null;
			if(storeId != null){
				storeIdL = Long.parseLong(storeId);
			}
			if (platformId != null){
				platformIdL = Long.parseLong(platformId);
			}
			// check if the auth token user mapping is correct
			boolean primaryCheck = false;
			// check if auth token and user header has not been manipulated
			boolean secondaryCheck = false;
			// flag to indicate if new table user_device_auth is being used or not for auth token
			Boolean withDeviceId=false;
			// check if body-userId is same as header-Auth-userId
			boolean userIdCheck = false;

			boolean authTokenExpired = false;
			Long userId = null;
			UserAuthDTO userAuth = null;
			String bodyUserId = null;
			if (authCredentials != null && !authCredentials.equalsIgnoreCase("null") && headerUserId != null) {
				String authToken = null;
				try {
					authCredentials = Crypt.decrypt(authCredentials);
					userId = Long.parseLong(authCredentials.split("!!")[0]);
					authToken = authCredentials.split("!!")[1];
				} catch (Exception ex) {
					logger.error("Exception while decrypting auth header" + ex);
				}

				if (userId != null && authToken != null) {
					if(storeIdL == null){
						storeIdL = ssoService.getRequestStore(httpServletRequest);
					}
					try {
						if (EnumPlatformType.ANDROID.getId().equals(platformIdL) && StoreConstants.DEFAULT_STORE_ID.equals(storeIdL)) {
							// this is for debugging requests from users who are having an issue
							String userIds = ssoService.getUserIdsToPrintLogs(storeIdL);
							if (userIds != null && !userIds.isEmpty() && userIds.contains(headerUserId)) {
								logger.info("In SecureAuthFilter, for userId " + headerUserId + " and for device "+deviceId
										+" and for token "+authToken+" and for store " + storeId + " platform " + platformIdL +
										" and appVersion " + version + " URL " + httpServletRequest.getRequestURL() +
										" and Method " + httpServletRequest.getMethod() + "and user-agent:" + userAgent +
										"  and Content type " + httpServletRequest.getContentType());
							}
						}
					}catch(Exception e){
						logger.info("Exception while printing logs for user: "+ headerUserId,e);
					}

					withDeviceId = ssoService.getUserDeviceAuthStore(storeIdL);

					if (storeIdL == null || !withDeviceId) {
						// default to old method of authentication
						userAuth = ssoService.getUserAuthByToken(authToken);
					} else {
						// use the new table user_device_auth
						userAuth = ssoService.getAuthToken(authToken, deviceId, httpServletRequest,
								Long.parseLong(headerUserId), storeIdL, platformIdL,version,userAgent);
					}

					User agent = null;
					if (userAuth != null) {
						if (userAuth.getExpiryDate().after(new Date())) {
							Long userAuthUserId = userAuth.getUserId();
							if (userAuthUserId.equals(userId)) {
								primaryCheck = true;
							} else {
								agent = agentHasUserService.getAgentMappedToUser(userId, null);
								primaryCheck = isAgentOrB2BEmployee(agent,userAuthUserId,userId);
							}
							if(withDeviceId){
								if(!userAuth.getStore().equals(storeIdL)){
									primaryCheck=false;
								}
							}
						} else {
							authTokenExpired = true;
						}
					}
					if (primaryCheck) {
						if (String.valueOf(userId).equals(headerUserId)) {
							secondaryCheck = true;
						} else {
							if (agent == null) {
								agent = agentHasUserService.getAgentMappedToUser(Long.parseLong(headerUserId), null);
							}
							secondaryCheck =  isAgentOrB2BEmployee(agent,userId,userId);
						}
					}

					// for post request, check the userId body param with userId
					// header param
					if (primaryCheck && secondaryCheck && httpServletRequest.getMethod().equalsIgnoreCase("POST")) {
						try {
							bodyUserId = compareHeaderUIdAndBodyUId(userId, httpServletRequest);
							if(StringUtils.isNotBlank(bodyUserId)) {
								if(USER_ID_DOESNT_EXISTS.equals(bodyUserId)){
									userIdCheck = true;
								}else if (String.valueOf(headerUserId).equals(bodyUserId)) {
									userIdCheck = true;
								} else {
									agent = agentHasUserService.getAgentMappedToUser(Long.parseLong(bodyUserId), null);

									userIdCheck = isAgentOrB2BEmployee(agent, Long.parseLong(headerUserId), Long.parseLong(headerUserId));
								}
							}
						} catch (Exception e) {
							logger.error("Error while checking body-userId with header-auth-userId for :" + userId);
						}
					} else if (httpServletRequest.getMethod().equalsIgnoreCase("GET")) {
						userIdCheck = true;
					}
				}
			}

			if (!primaryCheck || !secondaryCheck || !userIdCheck) {
				String COMMON_LOGS = " | For API : " + httpServletRequest.getRequestURL().toString()
						+ " | HEADER_USER_ID : " + headerUserId + " | AUTH_USER_ID : " + userId + " | PLATFORM : "
						+ platformId + "| APP_VERSION : " + version;

				if (!primaryCheck) {
					if (authTokenExpired) {
						logger.error(UNAUTH_MSG_TEMPLETE + " | USER_AUTH_TOKEN_EXPIRED | AUTH_TOKEN_EXPIRY DATE : "
								+ userAuth.getExpiryDate() + COMMON_LOGS);
					} else if(httpServletRequest.getRequestURL().toString().contains("/v3/logout")
							&& EnumPlatformType.IOS.getId().equals(platformIdL)){
						logger.error(UNAUTH_MSG_TEMPLETE + " | V3 LOGOUT CALL Not allowed | " + authCredentials + COMMON_LOGS);
						throw new ZuulException("Not allowed", HttpServletResponse.SC_FORBIDDEN, "Not allowed");
					} else {
						logger.error(UNAUTH_MSG_TEMPLETE + " | USER_AUTH_MISMATCH | USER_AUTH_TOKEN_FROM_CLIENT : "
								+ authCredentials + COMMON_LOGS);
					}
				} else if (!secondaryCheck) {
					logger.error(UNAUTH_MSG_TEMPLETE + " | AUTH_USER_ID & HEADER_USER_ID MISMATCH  " + COMMON_LOGS);
				} else {
					logger.error(UNAUTH_MSG_TEMPLETE + "| BODY_USER_ID & HEADER_USER_ID MISMATCH  | BODY_USER_ID : "
								+ bodyUserId +" HEADER_USER_ID : "+headerUserId+" " + COMMON_LOGS);
				}
				throw new ZuulException("Unauthorized", HttpServletResponse.SC_UNAUTHORIZED, "UNAUTHORIZED");
			}
		}

		return null;
	}

	private boolean isAgentOrB2BEmployee(User agent,Long userAuthUserId,Long userId){
		return agent != null && agent.getId().equals(userAuthUserId)
				|| userService.userHasRole(userId, RoleConstants.B2B_EMPLOYEE);
	}

	@Override
	public String filterType() {
		return PRE_TYPE;
	}

	@Override
	public int filterOrder() {
		return PRE_DECORATION_FILTER_ORDER;
	}

	private String compareHeaderUIdAndBodyUId(Long headerUserId, HttpServletRequest httpServletRequest)
			throws IOException {
		String httpRequestBody = null;
		String bodyUserId = null;
		try {
			httpRequestBody = IOUtils.toString(httpServletRequest.getInputStream(), StandardCharsets.UTF_8);
			JSONObject jsonObject = new JSONObject(httpRequestBody);
			if(jsonObject.has("userId")) {
				bodyUserId = jsonObject.get("userId").toString();
			}else{
				return USER_ID_DOESNT_EXISTS;
			}
		} catch (Exception e) {
			/*logger.error(UNAUTH_MSG_TEMPLETE
					+ " | NOT GETTING USER_ID IN POST BODY ERROR WHILE PARSING REQUEST | " +
					"For API : " + httpServletRequest.getRequestURL() + "  For User-Id[HEADER] : " + headerUserId);*/
			return USER_ID_DOESNT_EXISTS;
		}
		return bodyUserId;
	}

}
