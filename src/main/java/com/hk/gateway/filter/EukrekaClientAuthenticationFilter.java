package com.hk.gateway.filter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EukrekaClientAuthenticationFilter{/*// implements Filter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Value("${auth.user:}")
	private String eurekaAuthUser;

	@Value("${auth.password:}")
	private String eurekaAuthPassword;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = ((HttpServletRequest) servletRequest);
		String sb = request.getRequestURI();
		log.error("filter:" + sb);
		if (sb.equals("/") || sb.contains("eureka")) {
			String authHeader = request.getHeader("AUTHORIZATION");
			log.error("Auth header :" + authHeader);
			if (authHeader == null) {
				HttpServletResponse response = ((HttpServletResponse) servletResponse);
				response.setHeader("WWW-Authenticate", "Basic realm=\"User Visible Realm\"");
				response.sendError(401);
				log.error("no authHeader found");
				return;
			}
			String base64Credentials = authHeader.substring("Basic".length()).trim();
			if (StringUtils.isEmpty(base64Credentials)) {
				HttpServletResponse response = ((HttpServletResponse) servletResponse);
				response.setHeader("WWW-Authenticate", "Basic realm=\"User Visible Realm\"");
				response.sendError(401);
				log.error("no base64Credentials");
				return;
			}
			String credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8"));
			if (StringUtils.isEmpty(credentials) || !credentials.contains(":")) {
				HttpServletResponse response = ((HttpServletResponse) servletResponse);
				response.setHeader("WWW-Authenticate", "Basic realm=\"User Visible Realm\"");
				response.sendError(401);
				log.error("no Credentials");
				return;
			}

			final String[] values = credentials.split(":", 2);
			if (!values[0].equals(eurekaAuthUser) || !values[1].equals(eurekaAuthPassword)) {
				HttpServletResponse response = ((HttpServletResponse) servletResponse);
				response.setHeader("WWW-Authenticate", "Basic realm=\"User Visible Realm\"");
				response.sendError(401);
				log.error("Credentials dont match");
				return;
			}
			log.error("credentails "+values[0]+" "+values[1]);
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void destroy() {

	}

*/}
