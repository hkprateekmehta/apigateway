package com.hk.gateway.filter;

import com.hk.gateway.constant.GatewayApplicationConstants;
import com.hk.gateway.constant.RequestConstants;
import com.hk.gateway.response.SecurityResponseObj;
import com.hk.gateway.service.SecurityBypassedService;
import com.hk.gateway.service.SecurityService;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.hk.gateway.constant.StoreConstants;
import com.hk.gateway.pojo.StoreConfigValues;
import com.hk.gateway.repo.StoreConfigValuesRepo;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Set;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

public class HKSecurityFilter extends ZuulFilter {

    private static Logger logger = LoggerFactory.getLogger(HKSecurityFilter.class);

    @Autowired
    private SecurityService securityService;

    @Autowired
    private SecurityBypassedService securityBypassedService;
    
    @Autowired
    private StoreConfigValuesRepo storeConfigValuesRepo;

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return PRE_DECORATION_FILTER_ORDER;
    }

    public boolean shouldFilter() {
        String requestURI = RequestContext.getCurrentContext().getRequest().getRequestURI();
        return requestURI.startsWith(GatewayApplicationConstants.API_REQUEST_URI) || requestURI.startsWith(GatewayApplicationConstants.LOGISTICS_REQUEST_URI);
    }

    public Object run() throws ZuulException {
        RequestContext context = RequestContext.getCurrentContext();
        ServletRequest request = context.getRequest();

        if (request !=null) {
            if (securityService == null) {
                ServletContext servletContext = request.getServletContext();
                WebApplicationContext webApplicationContext = WebApplicationContextUtils
                        .getWebApplicationContext(servletContext);
                if(webApplicationContext!=null) {
                    securityService = webApplicationContext.getBean(SecurityService.class);
                }
            }


            if (!securityService.checkUseHkSecurity()) {
                return null;
            }
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            String requestURI = httpServletRequest.getRequestURI();
            String clientId = httpServletRequest.getHeader(RequestConstants.CLIENT_ID);
            String clientIP = httpServletRequest.getHeader(RequestConstants.X_FORWARDED_FOR);
            if (clientIP == null || "".equals(clientIP)) {
                clientIP = request.getRemoteAddr();
            }
            String clientAPI = httpServletRequest.getRequestURL().toString();
            String authCredentials = httpServletRequest.getHeader(RequestConstants.AUTHENTICATION_HEADER);

            SecurityResponseObj securityResponseObj = securityService.getSecurityResponseObj();
            /*String prefix=clientAPI.substring(clientAPI.indexOf("/")+1,clientAPI.indexOf("/") + 2);*/
            String prefix = clientAPI.substring(StringUtils.ordinalIndexOf(clientAPI, "/", 3), StringUtils.ordinalIndexOf(clientAPI, "/", 4));
            switch (prefix) {
                case "/logistics":
                    securityResponseObj = securityService.getLogisticsResponseObj();
                    break;
            }

            String clientAPITrimmed = clientAPI.substring(StringUtils.ordinalIndexOf(clientAPI, "/", 3));
            String[] clientAPIArray = clientAPITrimmed.split("/");
            int clientAPIArrayLength = clientAPIArray.length;

            // for crm calls - if auth header is present then only SecureAuthFilter
            // should run and hksecurity filter should be bypassed
            if (StringUtils.isNotBlank(authCredentials)) {
                if((requestURI.contains("/v3/") && requestURI.startsWith("/api/")) || (securityBypassedService.checkForSecurityBypass(requestURI,clientAPIArrayLength,clientAPIArray))) {
                    return null;
                }
            }

            if (securityResponseObj != null) {

                Map<Integer, Set<String>> nonStarOpenApiMap = securityResponseObj.getNonStarOpenApiMap();
                //logger.info("nonStarOpenApiMap");
                //nonStarOpenApiMap.forEach((k,v)->{logger.info(k+" "+v);});

                if (securityService.checkClientApiInNonStarOpenApi(clientAPITrimmed,
                        clientAPIArrayLength, nonStarOpenApiMap)) {
                    return null;
                }

                Map<Integer, Set<String>> starOpenApiMap = securityResponseObj.getStarOpenApiMap();
                //logger.info("nonStarOpenApiMap");
                //starOpenApiMap.forEach((k,v)->{logger.info(k+" "+v);});
                if (securityService.checkWithStarOpenApi(clientAPIArray, clientAPIArrayLength, starOpenApiMap)) {
                    return null;
                }

                if (clientId == null) {
                    logger.info("CLIENT IP : " + clientIP + "=== clientId is NULL Request UNAUTHORIZED for url : === "
                            + clientAPI);
                    setForbiddenError();
                    return null;
                }
                
                StoreConfigValues value = storeConfigValuesRepo.findByConfigKeyAndStoreId(StoreConstants.HAPTIK_CLIENT_ID,StoreConstants.DEFAULT_STORE_ID);
                if(value==null || !clientId.equals(value.getConfigValue())){
                Map<String, Set<String>> clientIdAndClientIPsMapping = securityResponseObj.getClientIdAndClientIPsMapping();
                if (clientIdAndClientIPsMapping != null && !clientIdAndClientIPsMapping.isEmpty()) {
                    if (!securityService.checkClientIP(clientId, clientIP, clientIdAndClientIPsMapping)) {
                        logger.info("Invalid IP : " + clientIP + "=== clientId : " + clientId
                                + "=== clientIP does not exists in security === Request UNAUTHORIZED for url : === "
                                + clientAPI);
                        setForbiddenError();
                        return null;
                    }
                  }
                }

                Map<String, Map<Integer, Set<String>>> nonStarClientIdAndClientAPIsMapping = securityResponseObj.getNonStarClientIdAndClientAPIsMapping();
                if (nonStarClientIdAndClientAPIsMapping != null && nonStarClientIdAndClientAPIsMapping.size() > 0) {
                    if (securityService.checkNonStarInternalAPI(clientId, clientAPITrimmed, clientAPIArrayLength, nonStarClientIdAndClientAPIsMapping)) {
                        return null;
                    }
                }
                Map<String, Map<Integer, Set<String>>> starClientIdAndClientAPIsMapping = securityResponseObj.getStarClientIdAndClientAPIsMapping();
                if (starClientIdAndClientAPIsMapping != null && starClientIdAndClientAPIsMapping.size() > 0) {
                    if (securityService.checkStarInternalApi(clientId, clientAPIArray, clientAPIArrayLength, starClientIdAndClientAPIsMapping)) {
                        return null;
                    }

                }
                logger.info("clientId : " + clientId + "=== clientIp : " + clientIP
                        + "=== Request UNAUTHORIZED for url : === " + clientAPI);
                setForbiddenError();
                return null;

            } else {
                logger.info("clientId : " + clientId + "=== clientIp : " + clientIP
                        + "=== securityResponseObj is NULL Request UNAUTHORIZED for url : === " + clientAPI);
                setForbiddenError();
                return null;
            }
        }

        return null;
    }

    private void setForbiddenError() throws ZuulException {
        throw new ZuulException("Forbidden", HttpServletResponse.SC_FORBIDDEN, "FORBIDDEN");
    }

}

