package com.hk.gateway.filter;

import com.hk.gateway.constant.GatewayApplicationConstants;
import com.hk.gateway.constant.RequestConstants;
import com.hk.gateway.response.SecurityResponseObj;
import com.hk.gateway.service.SecurityService;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Set;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

public class CrmHKSecurityFilter extends ZuulFilter {

    private static Logger logger = LoggerFactory.getLogger(CrmHKSecurityFilter.class);


    @Autowired
    private SecurityService securityService;

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return PRE_DECORATION_FILTER_ORDER;
    }

    public boolean shouldFilter() {
        String requestURI = RequestContext.getCurrentContext().getRequest().getRequestURI();
        return requestURI.startsWith(GatewayApplicationConstants.CRM_REQUEST_URI);
    }

    public Object run() throws ZuulException {
        RequestContext context = RequestContext.getCurrentContext();
        ServletRequest request = context.getRequest();
        if (request !=null) {
            loadSecurityService(request);

            if (securityService==null || !securityService.checkUseHkSecurity()) {
                return null;
            }
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            String clientId = httpServletRequest.getHeader(RequestConstants.CLIENT_ID);
            String clientIP = httpServletRequest.getHeader(RequestConstants.X_FORWARDED_FOR);
            if (clientIP == null || StringUtils.isBlank(clientIP)) {
                clientIP = request.getRemoteAddr();
            }
            String clientAPI = httpServletRequest.getRequestURL().toString();
            String authCredentials = httpServletRequest.getHeader(RequestConstants.AUTHENTICATION_HEADER);
            // should run and hksecurity filter should be bypassed
            if (StringUtils.isNotBlank(authCredentials)) {
                return null;
            }
            SecurityResponseObj securityResponseObj = securityService.getCrmSecurityResponseObj();
            /*String prefix=clientAPI.substring(clientAPI.indexOf("/")+1,clientAPI.indexOf("/") + 2);
            String prefix = clientAPI.substring(StringUtils.ordinalIndexOf(clientAPI, "/", 3), StringUtils.ordinalIndexOf(clientAPI, "/", 4));*/


            if (securityResponseObj != null) {
                String clientAPITrimmed = clientAPI.substring(StringUtils.ordinalIndexOf(clientAPI, "/", 3));
                String[] clientAPIArray = clientAPITrimmed.split("/");
                int clientAPIArrayLength = clientAPIArray.length;
                Map<Integer, Set<String>> nonStarOpenApiMap = securityResponseObj.getNonStarOpenApiMap();

                if (securityService.checkClientApiInNonStarOpenApi(clientAPITrimmed,
                        clientAPIArrayLength, nonStarOpenApiMap)) {
                    return null;
                }

                Map<Integer, Set<String>> starOpenApiMap = securityResponseObj.getStarOpenApiMap();
                if (securityService.checkWithStarOpenApi(clientAPIArray, clientAPIArrayLength, starOpenApiMap)) {
                    return null;
                }

                if (StringUtils.isNotBlank(clientId)) {

                /*Map<String, Set<String>> clientIdAndClientIPsMapping = securityResponseObj.getClientIdAndClientIPsMapping();
                if (clientIdAndClientIPsMapping != null) {
                    if (!checkClientIP(clientId, clientIP, clientIdAndClientIPsMapping)) {
                        logger.info("Invalid IP : " + clientIP + "=== clientId : " + clientId
                                + "=== clientIP does not exists in security === Request UNAUTHORIZED for url : === "
                                + clientAPI);
                        setForbiddenError();
                        return null;
                    }
                }*/

                    Map<String, Map<Integer, Set<String>>> nonStarClientIdAndClientAPIsMapping = securityResponseObj.getNonStarClientIdAndClientAPIsMapping();
                    if (nonStarClientIdAndClientAPIsMapping != null && nonStarClientIdAndClientAPIsMapping.size() > 0) {
                        if (securityService.checkNonStarInternalAPI(clientId, clientAPITrimmed, clientAPIArrayLength, nonStarClientIdAndClientAPIsMapping)) {
                            return null;
                        }
                    }
                    Map<String, Map<Integer, Set<String>>> starClientIdAndClientAPIsMapping = securityResponseObj.getStarClientIdAndClientAPIsMapping();
                    if (starClientIdAndClientAPIsMapping != null && starClientIdAndClientAPIsMapping.size() > 0) {
                        if (securityService.checkStarInternalApi(clientId, clientAPIArray, clientAPIArrayLength, starClientIdAndClientAPIsMapping)) {
                            return null;
                        }

                    }
                }
                logger.info("clientId : " + clientId + "=== clientIp : " + clientIP
                        + "=== Request UNAUTHORIZED for url : === " + clientAPI);
                setForbiddenError();
                return null;

            } else {
                logger.info("clientId : " + clientId + "=== clientIp : " + clientIP
                        + "=== securityResponseObj is NULL Request UNAUTHORIZED for url : === " + clientAPI);
                setForbiddenError();
                return null;
            }
        }

        return null;
    }







    /*private boolean checkClientIP(String clientId, String clientIP, Map<String, Set<String>> clientIdAndClientIPsMapping) {
        Set<String> clientIps = clientIdAndClientIPsMapping.get(clientId);

        String clientIPArray[] = clientIP.split(",");
        if (clientIps != null && clientIps.size() > 0) {
            for (String clientIP3 : clientIPArray) {
                int endIndex = clientIP3.lastIndexOf(".");
                clientIP3 = clientIP3.substring(0, endIndex);
                if (clientIps.contains(clientIP3)) {
                    return true;
                }
            }
        }
        return false;

    }*/





    private void loadSecurityService(ServletRequest request){
        if (securityService == null) {
            ServletContext servletContext = request.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils
                    .getWebApplicationContext(servletContext);
            if(webApplicationContext!=null) {
                securityService = webApplicationContext.getBean(SecurityService.class);
            }
        }
    }

    private void setForbiddenError() throws ZuulException {
        throw new ZuulException("Forbidden", HttpServletResponse.SC_FORBIDDEN, "FORBIDDEN");
    }

}

