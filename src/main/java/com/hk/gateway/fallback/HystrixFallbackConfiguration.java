package com.hk.gateway.fallback;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;

import com.hk.gateway.client.SecurityFeignClient;
import com.hk.gateway.response.SecurityResponseObj;

@Configuration
public class HystrixFallbackConfiguration implements SecurityFeignClient {

	@Value("${zuul.routes.hkSecurity.service-id}")
	private String securityRoute;

	@Value("${zuul.routes.api.service-id}")
	private String apiRoute;

	@Override
	public SecurityResponseObj getApplicationConfiguration(String applicationId) {
		System.out.println(" feign fallback");
		return null;
	}

	//@Bean
	public FallbackProvider zuulFallbackProvider() {
		return new FallbackProvider() {

			public String getRoute() {
				// Might be confusing: it's the serviceId property and not the
				// route
				return apiRoute;
			}

			public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
				return new ClientHttpResponse() {

					public HttpHeaders getHeaders() {
						HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.APPLICATION_JSON);
						return headers;
					}

					public InputStream getBody() throws IOException {
						return new ByteArrayInputStream("{Sorry Service is Down api-service}".getBytes());
					}

					public String getStatusText() throws IOException {
						return HttpStatus.OK.toString();
					}

					public HttpStatus getStatusCode() throws IOException {
						return HttpStatus.OK;
					}

					public int getRawStatusCode() throws IOException {
						return HttpStatus.OK.value();
					}

					public void close() {
						// TODO Auto-generated method stub

					}
				};
			}

		};
	}

	// this is route specific fallback in case of connect or request timeout it will be called for all urls of this route 
	//@Bean
	public FallbackProvider securityFallback() {
		return new FallbackProvider() {

			public String getRoute() {
				// Might be confusing: it's the serviceId property and not the
				// route
				return securityRoute;
			}

			public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
				return new ClientHttpResponse() {

					public HttpHeaders getHeaders() {
						HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.APPLICATION_JSON);
						return headers;
					}

					public InputStream getBody() throws IOException {
						return new ByteArrayInputStream("{Sorry Service is Down security-service}".getBytes());
					}

					public String getStatusText() throws IOException {
						return HttpStatus.OK.toString();
					}

					public HttpStatus getStatusCode() throws IOException {
						return HttpStatus.OK;
					}

					public int getRawStatusCode() throws IOException {
						return HttpStatus.OK.value();
					}

					public void close() {
						// TODO Auto-generated method stub

					}
				};
			}

		};
	}

}