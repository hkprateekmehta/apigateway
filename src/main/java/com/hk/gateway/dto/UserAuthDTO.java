package com.hk.gateway.dto;


import java.util.Date;

public class UserAuthDTO {

  private Long userId;
  private String authToken;
  private Date expiryDate;
  private Long store;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getAuthToken() {
    return authToken;
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }

  public Date getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  public Long getStore() {
    return store;
  }

  public void setStore(Long store) {
    this.store = store;
  }
}
